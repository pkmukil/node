var mysql = require('mysql');
// var pool;

// module.exports = {
// 	getPool: function () {
// 		if (pool)
// 			return pool;
// 		pool = mysql.createPool({
// 			host: 'localhost',
// 			user: 'root',
// 			password: '',
// 			database: 'vicky'
// 		})
// 		return pool;
// 	}
// }



// mysql.createPool({
// 	host: 'localhost',
// 	user: 'root',
// 	password: '',
// 	database: 'vicky'
// }).getConnection(function (err, connection) {
// 	if (err) throw err; // not connected!
// 	// Use the connection
// 	connection.query('SELECT * FROM logins', function (error, results, fields) {
// 		console.log(results);
// 		// When done with the connection, release it.
// 		connection.release();

// 		// Handle error after the release.
// 		if (error) throw error;

// 		// Don't use the connection here, it has been returned to the pool.
// 	});
// });
// module.exports = mysql;



// var db;
// function connectDatabase() {
// 	if (!db) {
// 		db = mysql.createPool({
// 			host: 'localhost',
// 			user: 'root',
// 			password: '',
// 			database: 'vicky'
// 		});
// 		db.getConnection(function (err, connection) {

// 			if (!err) {
// 				console.log('Database is connected!');
// 			} else {
// 				console.log('Error connecting database!');
// 			}

// 		});
// 	}
// 	return db;
// }
// module.exports = connectDatabase();



var sqlConnection = function sqlConnection(sql, values, next) {

	// It means that the values hasnt been passed
	if (arguments.length === 2) {
		next = values;
		values = null;
	}

	var connection = mysql.createConnection({
		host: 'localhost',
		user: 'root',
		password: '',
		database: 'mobi_data'
	});
	connection.connect(function (err) {
		if (err !== null) {
			console.log("[MYSQL] Error connecting to mysql:" + err + '\n');
		}
	});

	connection.query(sql, values, function (err) {

		connection.end(); // close the connection

		if (err) {
			// next.apply();
			// throw err;
		}
		
		// Execute the callback
		next.apply(this, arguments);

	});
}

module.exports = sqlConnection;