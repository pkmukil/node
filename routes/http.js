const express = require('express');
const router = express.Router();
const vickydb = require('./db');
const vickydb2 = require('./db2');
var async = require('async');
var Promise = require('promise');
/*var common_class = require('./common_class');

router.get('/common_class', async (req, res) => {
    try {
        res.status(200).json({
            data: common_class.logs
        });
    } catch (err) {
        res.status(400).json({
            message: 'Some error occured',
            err:err
        });
    }
});*/
function query_data(query, callback) {
    pool = vickydb2.getPool();
    pool.getConnection(function(err, connection) {
        if (err) return callback('connection error');
        connection.query(query, function(err, rows) {
            connection.release();
            if (err) return callback('query error');
            return callback(rows);
        });
    });
}

router.get('/db2/:id', async(req, res) => {
    console.log(req.params);
    result = [];
    result = await query_data('select * from device_details where 1') //, function (res) {
        //     console.log(res);
        //     result = res;
        // });
    console.log(result);
    res.status(200).json({
        data: result
    });
});


router.get('/', async(req, res) => {
    vickydb('SELECT * from device_details where ?', { sno: '1' }, function(err, rows) {
        if (err) {
            res.status(400).json({
                message: 'Some error occured',
                err: err.message
            });
            return false;
        }
        res.status(200).json({
            data: rows
        });
    });

});





function readDb(req) {
    return new Promise(function(resolve, reject) {
        var result = {}
        var qry = 'SELECT * from login where username="' + req.params.name + '" and password="' + req.params.password + '"';
        vickydb(qry, function(err, rows) {
            if (err) {
                throw new Error('BROKEN')
            }
            if (rows.length > 0) {
                result['state'] = 'ok';
                result['project'] = rows[0]['project'];
                result['name'] = rows[0]['name'];
                result['mobile'] = rows[0]['mobile'];
                result['level'] = rows[0]['level'];
                result['uname'] = rows[0]['username'];
                result['image'] = rows[0]['image'];
                result['mail'] = rows[0]['email'];
                result['toast'] = rows[0]['toast'];
                result['a1'] = 0;
                result['device_flow'] = 0;
                result['pressure_flow'] = 0;
                result['a_level'] = 0;
            } else {
                result['state'] = 'Failed';
            }
            resolve(result);
        });
    });
}

function deviceDb(data) {
    try {
        return new Promise(function(resolve, reject) {

            var sql = 'SELECT * from device_details where 1';
            vickydb(sql, function(error, device_res) {
                if (error) {
                    throw error;
                }
                var product = [];
                for (row in device_res) {
                    if (device_res[row]) {
                        if (!product.includes(device_res[row]['device_type']))
                            product.push(device_res[row]['device_type'])

                    }
                    if ((device_res[row]['project'] == '' || device_res[row]['project'] == null) && device_res[row]['level'] != '0') {
                        data['a1'] = 1;
                    }
                    if (device_res[row]['level'] == '11' && device_res[row]['device_flow'] == '1') {
                        data['device_flow'] = 1;
                    } else {
                        if (device_res[row]['device_type'] == "DCON") {
                            if (device_res[row]['p8'] != "" && device_res[row]['p8'] != null) {
                                data['device_flow'] = device_res[row]['p8'].split(',')[12]
                            }
                        }
                    }
                }
                data['device_type'] = product.toString();
                resolve(data)
            })

        })
    } catch (err) {
        throw err;
    }
}

router.get('/login1/username/:name/:password/:mobile', function(req, res, next) {
    try {
        var run = async(req) => {
            let data = '';
            data = await readDb(req);
            if (data['state'] == 'ok')
                data = await deviceDb(data);
            res.status(200).send(data);
        }
        run(req)
    } catch (err) {
        res.status(400).json({
            message: 'Some error occured',
            err: err.message
        });
        return false;
    }

});

router.get('/login/username/:name/:password/:mobile', async(req, res) => {
    var sql = 'SELECT * from login where username="' + req.params.name + '" and password="' + req.params.password + '"';
    vickydb(sql, function(err, rows) {
        if (err) {
            res.status(400).json({
                message: 'Some error occured',
                err: err.message
            });
            return false;
        }
        res.status(200).json({
            data: 'rows'
        });
        var result = {}
        result['state'] = 'ok';
        result['project'] = rows[0]['project'];
        result['name'] = rows[0]['name'];
        result['mobile'] = rows[0]['mobile'];
        result['level'] = rows[0]['level'];
        result['uname'] = rows[0]['username'];
        result['image'] = rows[0]['image'];
        result['mail'] = rows[0]['email'];
        result['toast'] = rows[0]['toast'];
        result['a1'] = 0;
        result['device_flow'] = 0;
        result['pressure_flow'] = 0;
        result['a_level'] = 0;
        result['device_type'] = '';
        var get_device_sql = '';
        if (rows[0]['level'] == 0) {
            get_device_sql = 'select * from device_details where 1 ';
        } else {
            get_device_sql = 'select * from device_details where p4 like "%' + req.params.mobile + '%" or sh_num like "%' + req.params.mobile + '%" ';
        }
        vickydb(get_device_sql, function(err, device_res) {
            if (err) {
                res.status(400).json({
                    message: 'Some error occured',
                    err: err.message
                });
                return false;
            }
            var product = [];
            for (row in get_device_sql) {
                if (device_res[row]) {
                    if (!product.includes(device_res[row]['device_type']))
                        product.push(device_res[row]['device_type'])

                }
            }
            result['device_type'] = product.toString();
            // res.send(result);

        });
        console.log(result);
    });
});

module.exports = router;